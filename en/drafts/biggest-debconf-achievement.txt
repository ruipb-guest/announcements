Name				Message
--------------------------------------------------------------------
Joachim Breitner (1)		During DebConf 5 we discussed the idea of a
				packaging team for Perl libraries; together with 
				the GNOME team this effort was pioneering the
				idea of collaborative package maintenance that 
				showed to be very effective.

Joachim Breitner (2)    	During DebConf 9 I made wanna-build, the Debian
				autobuilder support, use the edos-distcheck tool 
				to check if a package’s build dependency can be
				satisfied before trying to build them. This took
				load of the buildd admins, allowing them to
				concentrate on real build failures, and makes
				wanna-build smart enough to figure out the order
				of rebuilds by itself.

David Bremner			[During DebConf11] the Debian Perl team managed to
				convert almost 2000 packages from a monolithic
				subversion repository into individual git repositories.

Luca Capello			DebConf11 Events BoF, which generated a lot of
				discussion on various mailing lists 
				(debian-publicity@ and debian-www@ in primis) and
				in RL, and it was the beginning for the Debian
				Events Box creation (which is progressing slowly,
				but still...)

Luk Claes 			My first DebConf I got to really know what the
				Release Team was about. Not long after I joined
				the Release Team. Later DebConfs we recruited
				other team members ;-)

Gerfried Fuchs (1)		During debconf10 I squashed 77 RC bugs in
				stable (and I wasn't even in NY ...).  It
				was done in the light of the RCBW done 
				during debconf, http://wiki.debconf.org/wiki/RCBW

Gerfried Fuchs (2)		during debconf9 where I noticed that the
				video team slept in and did manage to get
				the first talk of one day onto the
				stream, which was the starting point in
				getting involved in the video team.  In
				dc11 I was setting up the
				external streaming server [...]

Raphael Hertzog			During debconf11, we improved
				dpkg-buildflags in order to support
				hardening build flags. Without this work, we
				would still not benefit from hardened builds.

Joey Hess			At DebConf3 in Oslo, I finally met the
				other Debian Installer developers
				gathered together in person, and after a
				week of challanging work, we achieved the
				first successful installation of Debian
				with it. This
				reinvigorated our team, leading to many
				new members, more rapid
				development, and many more developer
				gatherings. The resulting program
				has since been used to install Debian,
				and derivative distributions, on
				tens of millions of systems.

Karolina Kalic			During DebConf11 I've met my new, permanent,
				sponsor (Guido Trotter)
				and we have a great collaboration since then!

Asheesh Laroia (1)		During Debconf8, I wrote some
				improvements to piuparts. It doesn't
				appear those changes are actually in the
				main piuparts tree (!? aw well), but it
				introduced me to Lucas Nussbaum, QA 
				extraordinaire, and gave me the confidence
				that people in Debian trusted my 
				contributions.

Asheesh Laroia (2)		During Debconf8, I learned about one DD
				finding himself locked out of his room in
				the middle of the night. This let me to 
				understand that DDs are not just technically
				sharp and concerned with software freedom
				but also sometimes hapless.

Asheesh Laroia (3)		During Debconf11, I engaged in ongoing
				hacking to finish the new version of 
				mentors.debian.net. Debian now uses this to
				help new contributors submit high-quality
				packages to Debian, and Arno Töll has
				taken the reins on this project. Since
				then, 11 people have
				contributed patches to this codebase, and
				it is the basis for tracking
				sponsorship requests in the Debian bug
				tracking system.

Kevin Mark			I'm a FLOSS advocate who wanted to help Debian by
				helping make Debconf in NY run smoothly and I
				volunteered to do Front Desk, Registration, etc.
				It was great to work and socialize with other
				Debian contributers

Roland Mas			During DebConf9 I refactored the FusionForge
				VCS subsystem, so that alioth.debian.org could
				handle modern version control besides CVS and
				Subversion.

Gergely Nagy (1)		"I attended the dak BoF during DebConf11,
				and a year or so later, I'm about to deploy a
				dak installation at work, which is really cool."

Gergely Nagy (2)		"During DebConf11, I started to adopt dpatch, 
				and shortly afterwards, deprecated it.
				This was made possible by the QA-related
				talks at DC11, which strengthened my
				desire to rid the world of this
				beast."

Gergely Nagy (3)		"During DebConf11, I gave a talk on
				packaging for beginners. Half a year
				later, I still get occassional mail about
				it - my 15 minutes of fame lasting for
				months! YAY!"

Christian Perrier		In DebConf4, I spent days and nights improving
				the Debian Installer localization infrastructure.
				Eight years after, most of these achievements 
				are still used to help translating the
				installation system in more than 70 languages.

Alexander Reichle-Schmehl	One of the biggest achievements I made do Debian 
				happenend during DebConf6, when the publicity 
				team was founded, which now asks me about the 
				biggest achievements I did during a DebConf :)
				
Andreas Tille 			Even though I was a DD for two years when
				I attended Debconf 0  I felt that this
				event was the start of a big friendship.

Guido Trotter			During Debconf9 I gave a talk about our project.
				Then one of the developers present found it could
				be used to solve some of their problem at work.
				Soon his organization became one of the most 
				active contributors in our community.

Wouter Verhelst			My biggest achievement at any debconf was
				to actually *be* at debconf5, my first-ever
				debconf. At the time, I was having personal 
				issues, and was seriously considering
				resigning from the project. Someone (I
				forgot whom) suggested that I could make it to
				debconf5 as a sponsored attendee, and 
				eventually convinced me. The opportunity
				to see and meet all those people who were
				so passionate about Debian, to see and
				meet them in a different setting, revived
				my passion and fire for this community. I
				am convinced that I would not be
				involved with Debian anymore today if I
				hadn't been at debconf5.

Enrico Zini (1)			As DAM, Debconf is a priceless opportunity
				to handle sensitive matters
				face to face, in an appropriate setting.
				Over time there are several
				such issues accumulating that cannot
				easily be handled via email.

Enrico Zini (2)			During Debconf11 I could and did talk
				personally with DSA and everyone I
				could find running services on Debian
				machines, and managed to set up a
				coordination channel among all of us.
